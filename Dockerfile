FROM phentz/binder-utt-5f-2dconnected-5f-2dinnovation-2dia-5f-2dcourse-5f-2d2019-884007:870352c469d91ed6105f5b936248765b6d25bf5c

ARG NB_USER

RUN rm -r /home/${NB_USER}/*

COPY . /home/${NB_USER}

USER root
RUN chown -R ${NB_USER} /home/${NB_USER}/*
RUN chown -R ${NB_USER} /home/${NB_USER}/.git
USER ${NB_USER}
